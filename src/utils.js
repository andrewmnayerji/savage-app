import { AsyncStorage } from 'react-native';

const getToken = () => AsyncStorage.getItem('token');

const formatNumber = (num) => {
  if (!num) {
    return '';
  }
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,').split('.')[0];
};

module.exports = { formatNumber, getToken };
