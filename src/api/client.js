import { getToken } from '../utils';

const BASE_URL = 'https://api.savage-fitness.club/';

function performApiCall(path, method, body = null, handler) {
  const url = `${BASE_URL}${path}`;
  if (body) {
    body = JSON.stringify(body);
  }
  let headers = { Accept: 'application/json', 'Content-Type': 'application/json' };
  getToken()
    .then((token) => {
      if (token) {
        headers = { ...headers, Authorization: `Token ${token}` };
      }
    })
    .then(() => fetch(url, { method, headers, body })
      .then((resp) => resp.text()
        .then((responseBody) => {
          if (resp.status >= 500) {
            !!handler && handler({ error: 'An unexpected error has occurred' });
          }
          if (resp.status >= 400) {
            let result = null;
            try {
              result = JSON.parse(responseBody);
            } catch (error) {
              // If the response is somehow not JSON
              return !!handler && handler({ error: 'An unexpected response was received from the server' });
            }
            if (result.constructor === Array) {
              return !!handler && handler({ error: result[0] });
            }
            if (result.constructor === Object) {
              const keys = Object.keys(result);
              if (keys.length > 0) {
                const key = keys[0];
                if (result[key].constructor === Array) {
                  return !!handler && handler({ error: result[key][0] });
                }
                return !!handler && handler({ error: result[keys[0]] });
              }
            }
          }
          if (resp.status === 204) {
            // 204 No response
            return !!handler && handler(null);
          }
          // 2xx, 3xx, and any unhandled cases
          return !!handler && handler(JSON.parse(responseBody));
        }))
      .catch(() => !!handler && handler({ error: 'An unexpected error has occurred' })));
}

const performDelete = (path, handler = null) => performApiCall(path, 'DELETE', null, handler);
const performGet = (path, handler = null) => performApiCall(path, 'GET', null, handler);
const performPost = (path, body, handler = null) => performApiCall(path, 'POST', body, handler);

export { performGet, performPost, performDelete };
