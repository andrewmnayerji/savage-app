import { AsyncStorage } from 'react-native';
import { performDelete, performGet, performPost } from './client';

const clearToken = () => AsyncStorage.removeItem('token');

const login = ({ email, password }, handler = null) => performPost(
  'security/login/',
  { email, password, username: email },
  (resp) => {
    if (resp.key) {
      AsyncStorage.setItem('token', resp.key);
    }
    !!handler && handler(resp);
  }
);

const logout = (handler) => {
  performPost('security/logout/', null);
  clearToken();
  !!handler && handler();
};
const getUserInfo = (handler) => performGet('security/user-info/', handler);

const listCourses = (handler) => performGet('courses/', handler);

const confirmAttendance = (body, handler) => performPost('confirmations/', body, handler);
const deleteAttendanceConfirmation = (id, handler) => performDelete(`confirmations/${id}/`, handler);
const listAttendanceConfirmations = (handler) => performGet('confirmations/', handler);

const listSubscriptionPlans = (handler) => performGet('plans/', handler);

module.exports = {
  clearToken,
  confirmAttendance,
  deleteAttendanceConfirmation,
  getUserInfo,
  listAttendanceConfirmations,
  listCourses,
  listSubscriptionPlans,
  login,
  logout
};
