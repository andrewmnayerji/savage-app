import React from 'react';
import {
  BackHandler,
  Image, Text, TouchableOpacity, View
} from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { listSubscriptionPlans } from '../api/actions';
import { SubscriptionType, ViewName } from '../constants';
import { styles } from '../styles';
import { formatNumber } from '../utils';

class SubscriptionPlanView extends React.Component {
  constructor() {
    super();
    this.state = { plans: [] };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this.goBack());

    const { errorHandler, inFlightHandler } = this.props;
    inFlightHandler(true);
    listSubscriptionPlans((plans) => {
      if (!errorHandler(plans, ViewName.LANDING)) {
        plans = _.groupBy(_.orderBy(plans, ['sessions_per_week', 'number_of_sessions', 'number_of_people'], ['asc', 'asc', 'asc']), (o) => o.type);
        if (!errorHandler(plans, ViewName.LANDING)) {
          this.setState({ plans });
        }
        inFlightHandler(false);
      }
    });
  }

  goBack() {
    const { viewChangeHandler } = this.props;
    viewChangeHandler(ViewName.LANDING);
    return true;
  }

  render() {
    const { plans } = this.state;
    const subscriptionPlans = {};
    for (const key in SubscriptionType) {
      subscriptionPlans[SubscriptionType[key]] = plans[SubscriptionType[key]] || [];
    }

    const ptPlans = subscriptionPlans[SubscriptionType.PERSONAL_TRAINING];
    let numberOfPeopleOptions = [];
    const personalTrainingMatrix = {};
    const silhouettes = {};
    if (ptPlans) {
      numberOfPeopleOptions = [...new Set(ptPlans.map((p) => p.number_of_people))];
      const numberOfSessionsOptions = [...new Set(ptPlans.map((p) => p.number_of_sessions))];

      for (const i in numberOfSessionsOptions) {
        const numSessions = numberOfSessionsOptions[i];
        if (!(numSessions in personalTrainingMatrix)) {
          personalTrainingMatrix[numSessions] = {};
        }
        for (const j in numberOfPeopleOptions) {
          const numberOfPeople = numberOfPeopleOptions[j];
          if (!(numSessions in personalTrainingMatrix[numSessions])) {
            personalTrainingMatrix[numSessions][numberOfPeople] = {};
          }
          const subscriptionPlan = ptPlans.find(
            (p) => p.number_of_people === numberOfPeople && p.number_of_sessions === numSessions
          ) || {};
          personalTrainingMatrix[numSessions][numberOfPeople] = subscriptionPlan.price;
        }
      }
    }
    for (const i in numberOfPeopleOptions.sort()) {
      const option = numberOfPeopleOptions[i];
      const icons = [];
      for (let j = 0; j < option; j += 1) {
        icons.push(
          <View style={styles.silhouetteContainer} k>
            <Image
              key={j}
              style={styles.silhouette}
              resizeMode="contain"
              source={require('../../assets/silhouette.png')}
            />
          </View>
        );
      }
      silhouettes[option] = icons;
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.backButtonWrapper}
          onPress={() => this.goBack()}
        >
          <Image
            style={styles.backButton}
            resizeMode="contain"
            source={require('../../assets/back-button.png')}
          />
        </TouchableOpacity>

        <Text style={{ ...styles.header }}>PLANS</Text>
        {Object.keys(subscriptionPlans).filter(
          (p) => subscriptionPlans[p].length > 0
                && p !== SubscriptionType.PERSONAL_TRAINING
                && !!subscriptionPlans[p]
        ).map((plan) => (
          <View key={plan} style={styles.subscriptionPlanGroup}>
            <View style={styles.subscriptionPlanHeader}>
              <Text style={styles.subscriptionPlanTitle}>{plan.toUpperCase()}</Text>
              <Text style={styles.subscriptionPlanDuration}> / MONTH / LBP</Text>
            </View>
            <View style={styles.rowBreak}><Text /></View>
            {
              subscriptionPlans[plan].map((planDetails) => (
                <View style={styles.rowContainer} key={planDetails.id}>
                  <Text
                    style={{ ...styles.subscriptionPlanDetailCell, ...styles.bold }}
                  >
                    {planDetails.sessions_per_week}
                    {' '}
                    DAYS/ WEEK
                  </Text>
                  <View style={styles.rowGroup}>
                    <Text
                      style={{ ...styles.subscriptionPlanDetailCell, ...styles.rightAligned }}
                    >
                      {formatNumber(planDetails.price)}
                    </Text>
                    {/* <Text style={styles.currency}>L.L.</Text> */}
                  </View>
                </View>
              ))
            }
            <View style={styles.rowBreak}><Text /></View>
          </View>
        ))}
        {
                    Object.keys(ptPlans).length > 0
                    && (
                    <View style={styles.subscriptionPlanGroup}>
                      <View style={styles.subscriptionPlanHeader}>
                        <Text
                          style={styles.subscriptionPlanTitle}
                        >
                          {SubscriptionType.PERSONAL_TRAINING.toUpperCase()}
                        </Text>
                        <Text style={styles.subscriptionPlanDuration}> / LBP</Text>
                      </View>
                      <View style={styles.rowBreak}><Text /></View>
                      <View style={{ ...styles.rowContainer, height: '7%' }}>
                        <View style={styles.personalTrainingSessionCell} />
                        {numberOfPeopleOptions.map((num) => (
                          <View style={styles.personalTrainingDetailCell}>
                            {num ? silhouettes[num] : ''}
                          </View>
                        ))}
                      </View>
                      {
                        Object.keys(personalTrainingMatrix).map((numSessions) => (
                          <View
                            style={styles.personalTrainingRowContainer}
                            key={numSessions}
                          >
                            <Text
                              style={styles.personalTrainingSessionCell}
                            >
                              {`${numSessions} SESSIONS`}
                            </Text>
                            {Object.keys(personalTrainingMatrix[numSessions]).map((numPeople) => (
                              <Text
                                style={styles.personalTrainingDetailCell}
                                key={`${numSessions}${numPeople}`}
                              >
                                {formatNumber(personalTrainingMatrix[numSessions][numPeople])}
                              </Text>
                            ))}
                          </View>
                        ))
                      }
                    </View>
                    )
                }
      </View>
    );
  }
}

SubscriptionPlanView.propTypes = {
  errorHandler: PropTypes.func.isRequired,
  inFlightHandler: PropTypes.func.isRequired,
  viewChangeHandler: PropTypes.func.isRequired,
};

export default SubscriptionPlanView;
