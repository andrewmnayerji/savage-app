import React from 'react';
import {
  BackHandler,
  Image, ImageBackground, Text, TextInput, TouchableOpacity, View
} from 'react-native';
import PropTypes from 'prop-types';
import { styles } from '../styles';
import { ViewName } from '../constants';
import { login } from '../api/actions';

class LoginView extends React.Component {
  constructor() {
    super();

    const images = [
      require('../../assets/backgrounds/background1.png'),
      require('../../assets/backgrounds/background2.png'),
    ];

    this.state = {
      email: '',
      password: '',
      backgroundImage: images[Math.floor(Math.random() * 2)]
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this.goBack());
  }

  goBack() {
    const { viewChangeHandler } = this.props;
    viewChangeHandler(ViewName.LANDING);
    return true;
  }

  inputChangeHandler(field, value) {
    this.setState({ [field]: value });
  }

  render() {
    const {
      errorHandler, inFlightHandler, loginHandler
    } = this.props;
    const { email, password, backgroundImage } = this.state;
    const canLogin = !!email && !!password;

    return (
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
      >
        <TouchableOpacity
          style={styles.backButtonWrapper}
          onPress={() => this.goBack()}
        >
          <Image
            style={styles.backButton}
            resizeMode="contain"
            source={require('../../assets/back-button.png')}
          />
        </TouchableOpacity>

        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            resizeMode="contain"
            source={require('../../assets/savage-logo.png')}
          />
        </View>
        <View style={styles.rowBreak5}><Text /></View>
        <View style={styles.rowContainer}>
          <TextInput
            style={styles.inputButton}
            autoCompleteType="email"
            autoCapitalize="none"
            autoFocus
            keyboardType="email-address"
            textContentType="emailAddress"
            placeholderTextColor="#555555"
            onChangeText={(text) => this.inputChangeHandler('email', text)}
            value={email}
            placeholder="USERNAME"
          />
        </View>
        <View style={styles.rowContainer}>
          <TextInput
            style={styles.inputButton}
            secureTextEntry
            autoCapitalize="none"
            textContentType="password"
            placeholderTextColor="#555555"
            onChangeText={(text) => this.inputChangeHandler('password', text)}
            value={password}
            placeholder="PASSWORD"
          />
        </View>

        <TouchableOpacity
          style={{ ...styles.loginButtonWrapper, opacity: canLogin ? 1 : 0.3 }}
          onPress={() => {
            inFlightHandler(true);
            login({ email, password },
              (resp) => !errorHandler(resp) && loginHandler()) && inFlightHandler(false);
          }}
          disabled={!canLogin}
        >
          <Image
            style={styles.loginButton}
            resizeMode="contain"
            source={require('../../assets/login-button.png')}
          />
        </TouchableOpacity>

      </ImageBackground>
    );
  }
}

LoginView.propTypes = {
  errorHandler: PropTypes.func.isRequired,
  inFlightHandler: PropTypes.func.isRequired,
  loginHandler: PropTypes.func.isRequired,
  viewChangeHandler: PropTypes.func.isRequired,
};

export default LoginView;
