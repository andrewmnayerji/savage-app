import React from 'react';
import {
  BackHandler,
  Image, Text, TouchableOpacity, View
} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { styles } from '../styles';
import {
  confirmAttendance,
  deleteAttendanceConfirmation,
  listAttendanceConfirmations,
  listCourses
} from '../api/actions';
import { DayNumbers, ViewName } from '../constants';

const transformCourses = (courses) => {
  const objs = [];
  for (const i in courses) {
    const course = courses[i];
    for (const j in course.course_offerings) {
      const courseOffering = course.course_offerings[j];
      const obj = {
        id: courseOffering.id,
        course_id: course.id,
        course_name: course.name,
        capacity: course.capacity,
        next_session: courseOffering.next_session_date,
        confirmations: courseOffering.confirmed_attendees,
        day_of_week: DayNumbers[courseOffering.day_of_week],
        start_time: courseOffering.start_time,
        end_time: courseOffering.end_time,
        next_session_date_time: new Date(`${courseOffering.next_session_date}T${courseOffering.start_time}`),
        next_session_date_time_str: `${courseOffering.next_session_date}T${courseOffering.start_time}`,
        confirmation_id: null,
      };
      objs.push(obj);
    }
  }

  return _.groupBy(_.orderBy(objs, ['next_session_date_time'], ['asc']), (o) => o.next_session);
};

class CourseOfferingView extends React.Component {
  constructor() {
    super();
    this.state = {
      courses: [],
      selectedDateIndex: -1,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this.goBack());
    this.fetchData();
  }

  goBack() {
    const { viewChangeHandler } = this.props;
    viewChangeHandler(ViewName.LANDING);
    return true;
  }

  attachConfirmationsToCourses(courses, confirmations) {
    for (const i in confirmations) {
      const confirmation = confirmations[i];
      const confirmationDateTime = confirmation.start_date_time.slice(0, -6);
      const confirmationDateOnly = confirmationDateTime.split('T')[0];
      if (confirmationDateOnly in courses) {
        const course = courses[confirmationDateOnly].find(
          (c) => c.course_id === confirmation.course
            && c.next_session_date_time_str === confirmationDateTime
        );
        course.confirmation_id = confirmation.id;
      }
    }
    this.setState({ courses });
  }

  removeConfirmationFromCourse(courseOfferingId, nextSession) {
    const { courses } = this.state;
    const index = courses[nextSession].findIndex((c) => c.id === courseOfferingId);
    courses[nextSession][index].confirmation_id = null;
    this.setState({ courses });
  }

  confirmSessionAttendance(offeringId, course, startDateTime) {
    const { errorHandler, inFlightHandler } = this.props;
    const { courses } = this.state;
    inFlightHandler(true);
    confirmAttendance({ course, start_date_time: startDateTime }, (attendance) => {
      if (!errorHandler(attendance, null, () => {
        this.fetchData();
      })) {
        this.attachConfirmationsToCourses(courses, [attendance]);
        inFlightHandler(false);
      }
    });
  }

  fetchData() {
    const { errorHandler, inFlightHandler, user } = this.props;
    const { selectedDateIndex } = this.state;
    inFlightHandler(true);
    listCourses((courses) => {
      if (!errorHandler(courses, ViewName.LANDING)) {
        courses = transformCourses(courses);
        const dates = Object.keys(courses);
        let newDateIndex = -1;
        if (dates.length > 0) {
          if (selectedDateIndex === -1) {
            newDateIndex = 0;
          } else {
            newDateIndex = selectedDateIndex;
          }
        }
        inFlightHandler(false);
        this.setState({ courses, selectedDateIndex: newDateIndex });

        if (user) {
          inFlightHandler(true);
          listAttendanceConfirmations((confirmations) => {
            if (!errorHandler(confirmations, ViewName.LANDING)) {
              this.attachConfirmationsToCourses(courses, confirmations);
              inFlightHandler(false);
            }
          });
        }
      }
    });
  }

  deleteSessionAttendance(courseOffering) {
    const { errorHandler, inFlightHandler } = this.props;
    inFlightHandler(true);
    deleteAttendanceConfirmation(courseOffering.confirmation_id, (resp) => {
      if (!errorHandler(resp, null, () => {
        this.fetchData();
      })) {
        this.removeConfirmationFromCourse(courseOffering.id, courseOffering.next_session);
        inFlightHandler(false);
      }
    });
  }

  selectNextDate() {
    const { courses, selectedDateIndex } = this.state;
    if (Object.keys(courses).length - 1 > selectedDateIndex) {
      this.setState({ selectedDateIndex: selectedDateIndex + 1 });
    }
  }

  selectPreviousDate() {
    const { selectedDateIndex } = this.state;
    if (selectedDateIndex > 0) {
      this.setState({ selectedDateIndex: selectedDateIndex - 1 });
    }
  }

  render() {
    const { user } = this.props;
    const loggedIn = !!user;
    const {
      courses, selectedDateIndex
    } = this.state;
    const courseKeys = Object.keys(courses);
    const maxIndex = courseKeys.length - 1;
    const rightButtonDisabled = selectedDateIndex === maxIndex;
    const leftButtonDisabled = selectedDateIndex === 0;

    let selectedDateStr = '';
    let selectedDateSessions = [];
    if (selectedDateIndex > -1 && courseKeys.length > 0) {
      const selectedDate = courseKeys[selectedDateIndex];
      selectedDateSessions = courses[selectedDate];
      selectedDateStr = courses[selectedDate][0].next_session_date_time;
      selectedDateStr = moment.utc(selectedDateStr).format('dddd DD MMMM YYYY');
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.backButtonWrapper}
          onPress={() => this.goBack()}
        >
          <Image
            style={styles.backButton}
            resizeMode="contain"
            source={require('../../assets/back-button.png')}
          />
        </TouchableOpacity>
        <Text
          style={styles.header}
        >
          WELCOME
          {' '}
          {loggedIn ? user.first_name.toUpperCase() : ''}
        </Text>
        <View style={styles.rowBreak}><Text /></View>
        <View style={styles.rowContainer15}>
          <TouchableOpacity
            onPress={() => this.selectPreviousDate()}
            disabled={leftButtonDisabled}
          >
            <Image
              style={{
                ...styles.datePickerButton,
                ...(leftButtonDisabled ? styles.disabledButton : {})
              }}
              resizeMode="contain"
              source={require('../../assets/left-arrow.png')}
            />
          </TouchableOpacity>
          <Text style={styles.datePicker}>{selectedDateStr}</Text>
          <TouchableOpacity
            onPress={() => this.selectNextDate()}
            disabled={rightButtonDisabled}
          >
            <Image
              style={{
                ...styles.datePickerButton,
                ...(rightButtonDisabled ? styles.disabledButton : {})
              }}
              resizeMode="contain"
              source={require('../../assets/right-arrow.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.rowBreak}><Text /></View>
        {selectedDateSessions.map((offering) => {
          // eslint-disable-next-line max-len
          const rowStyle = offering.confirmation_id ? styles.confirmedCalendarRow : styles.calendarRow;
          // eslint-disable-next-line max-len
          const cellStyle = offering.confirmation_id ? styles.confirmedCalendarCell : styles.calendarCell;
          const remainingSpots = offering.capacity - offering.confirmations;
          const offeringStatus = offering.confirmation_id ? 'Booked' : `${remainingSpots} Spot${remainingSpots === 1 ? '' : 's'} left`;
          const confirmAttendanceFunction = () => this.confirmSessionAttendance(
            offering.id, offering.course_id, offering.next_session_date_time_str
          );
          const deleteAttendance = () => this.deleteSessionAttendance(offering);
          const onPress = offering.confirmation_id ? deleteAttendance : confirmAttendanceFunction;
          return (
            <TouchableOpacity
              key={offering.id}
              style={rowStyle}
              onPress={onPress}
              disabled={!loggedIn}
            >
              <Text style={cellStyle}>
                {moment(offering.start_time, 'HH:mm:ss').format('hh:mm a').toUpperCase()}
              </Text>
              <Text style={cellStyle}>{offering.course_name}</Text>
              <Text style={cellStyle}>{offeringStatus}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

CourseOfferingView.propTypes = {
  errorHandler: PropTypes.func.isRequired,
  inFlightHandler: PropTypes.func.isRequired,
  user: PropTypes.shape({
    first_name: PropTypes.string.isRequired
  }).isRequired,
  viewChangeHandler: PropTypes.func.isRequired,
};

export default CourseOfferingView;
