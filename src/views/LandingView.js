import React from 'react';
import {
  Image, Text, TouchableOpacity, View
} from 'react-native';
import PropTypes from 'prop-types';
import { styles } from '../styles';
import { ViewName } from '../constants';

const LandingView = (props) => (
  <View style={styles.container}>
    <View style={styles.logoContainer}>
      <Image
        style={styles.logo}
        resizeMode="contain"
        source={require('../../assets/savage-logo.png')}
      />
    </View>
    <View style={styles.rowBreak5}><Text /></View>
    <View style={styles.rowContainer30}>
      <TouchableOpacity
        style={styles.imageButton}
        onPress={() => props.viewChangeHandler(ViewName.SUBSCRIPTION_PLANS)}
      >
        <Image
          style={styles.imageButtonImage}
          resizeMode="contain"
          source={require('../../assets/plans-button.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.imageButton}
        onPress={() => props.viewChangeHandler(ViewName.COURSE_OFFERING)}
      >
        <Image
          style={styles.imageButtonImage}
          resizeMode="contain"
          source={require('../../assets/schedule-button.png')}
        />
      </TouchableOpacity>
    </View>
    <View style={styles.rowBreak}><Text /></View>
    <View style={styles.rowContainer30}>
      <TouchableOpacity
        style={styles.imageButton}
        onPress={() => props.viewChangeHandler(
          props.loggedIn ? ViewName.COURSE_OFFERING : ViewName.LOGIN
        )}
      >
        <Image
          style={styles.imageButtonImage}
          resizeMode="contain"
          source={require('../../assets/book-buttom.png')}
        />
      </TouchableOpacity>
    </View>
  </View>
);

LandingView.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  viewChangeHandler: PropTypes.func.isRequired,
};

export default LandingView;
