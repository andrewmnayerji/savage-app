import React from 'react';
import {
  Alert,
  Image,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity
} from 'react-native';
import Spinner from 'react-native-modal-spinner';
import { StatusBar } from 'expo-status-bar';
import LandingView from './LandingView';
import CourseOfferingView from './CourseOfferingView';
import { ViewName } from '../constants';
import { getToken } from '../utils';
import { styles } from '../styles';
import { clearToken, getUserInfo, logout } from '../api/actions';
import SubscriptionPlanView from './SubscriptionPlanView';
import LoginView from './LoginView';

class MainView extends React.Component {
  constructor() {
    super();
    this.state = {
      inFlight: false,
      selectedView: ViewName.LANDING,
      user: null,
    };
  }

  componentDidMount() {
    this.checkLogin();
  }

  changeView = (selectedView) => this.setState({ selectedView });

  checkLogin = () => getToken()
    .then((token) => {
      const loggedIn = !!token;
      if (loggedIn) {
        this.inFlightHandler(true);
        getUserInfo((user) => {
          if (!this.handleError(user)) {
            this.setState({ user });
            this.inFlightHandler(false);
          }
        });
      }
    });

  handleError = (resp, resolutionView, callBack) => {
    if (!!resp && 'error' in resp) {
      if (resp.error.startsWith('Invalid token')) {
        clearToken();
      } else {
        Alert.alert(
          '',
          resp.error,
          [
            {
              text: 'Go Back',
              onPress: () => {
                !!resolutionView && this.changeView(resolutionView);
                !!callBack && callBack();
                this.inFlightHandler(false);
              },
              style: 'cancel'
            },
          ],
          { cancelable: false }
        );
      }
      return true;
    }
    return false;
  };

  inFlightHandler = (inFlight) => this.setState({ inFlight })

  render() {
    const { inFlight, selectedView, user } = this.state;

    return (
      <KeyboardAvoidingView
        style={{
          width: '100%',
          height: '100%'
        }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        {
          selectedView === ViewName.LANDING && (
            <LandingView
              loggedIn={!!user}
              viewChangeHandler={this.changeView}
            />
          )
        }
        {
          selectedView === ViewName.COURSE_OFFERING && (
            <CourseOfferingView
              errorHandler={this.handleError}
              inFlightHandler={this.inFlightHandler}
              user={user}
              viewChangeHandler={this.changeView}
            />
          )
        }
        {
          selectedView === ViewName.SUBSCRIPTION_PLANS && (
            <SubscriptionPlanView
              errorHandler={this.handleError}
              inFlightHandler={this.inFlightHandler}
              viewChangeHandler={this.changeView}
            />
          )
        }
        {
          selectedView === ViewName.LOGIN && (
            <LoginView
              loginHandler={() => {
                this.checkLogin();
                this.changeView(ViewName.LANDING);
              }}
              errorHandler={this.handleError}
              inFlightHandler={this.inFlightHandler}
              viewChangeHandler={this.changeView}
            />
          )
        }
        {
          !!user && (
            <TouchableOpacity
              style={styles.logoutButtonWrapper}
              onPress={() => {
                this.inFlightHandler(true);
                logout(() => {
                  this.setState({
                    user: null,
                    selectedView: ViewName.LANDING
                  });
                  this.inFlightHandler(false);
                });
              }}
            >
              <Image
                style={styles.logoutButton}
                resizeMode="contain"
                source={require('../../assets/logout-button.png')}
              />
            </TouchableOpacity>
          )
        }
        <Spinner
          color="#534f50"
          size="large"
          visible={inFlight}
        />
        <StatusBar
          style="light"
        />

      </KeyboardAvoidingView>
    );
  }
}

export default MainView;
