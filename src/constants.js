const ViewName = {
  COURSE_OFFERING: 'course-offering',
  LANDING: 'landing',
  LOGIN: 'login',
  SUBSCRIPTION_PLANS: 'subscription-plans',
};

const SubscriptionType = {
  CLASSES: 'Classes',
  GROUP_TRAINING: 'Group Training',
  PERSONAL_TRAINING: 'Personal Training',
};

const DayNumbers = {
  // This is used to translate from The API's indexing to a string - do not touch it
  0: 'Monday',
  1: 'Tuesday',
  2: 'Wednesday',
  3: 'Thursday',
  4: 'Friday',
  5: 'Saturday',
  6: 'Sunday',
};

export { DayNumbers, SubscriptionType, ViewName };
