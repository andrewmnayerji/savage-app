import { StyleSheet } from 'react-native';
import * as Constants from 'expo-constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: Constants.statusBarHeight,
    paddingTop: '20%',
    paddingLeft: 10,
    paddingRight: 10,
  },
  imageBackground: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: Constants.statusBarHeight,
    paddingTop: '20%',
    paddingLeft: 10,
    paddingRight: 10,
    resizeMode: 'cover',
  },
  rowBreak5: {
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    position: 'relative',
    margin: 1,
    width: '100%',
    justifyContent: 'center',
    height: '5%',
  },
  rowBreak: {
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    position: 'relative',
    margin: 10,
    width: '100%',
    justifyContent: 'center',
    height: 10,
  },
  logoContainer: {
    flex: 3,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: '35%',
    width: '100%',
  },
  logo: {
    flex: 1,
    height: '100%',
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  justifiedRowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: '5%',
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  rowContainer30: {
    flex: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: '30%',
    width: '100%',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  rowContainer15: {
    flex: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: '15%',
    width: '100%',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  rowContainer5: {
    flex: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    height: '5%',
    width: '100%',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  imageButton: {
    flex: 1,
    height: '70%',
    marginLeft: 10,
    marginRight: 10,
  },
  imageButtonImage: {
    height: '100%',
    width: '100%'
  },
  logoutButtonWrapper: {
    position: 'absolute',
    top: '5%',
    right: '1%',
    width: 35,
    height: 35,
  },
  logoutButton: {
    width: 35,
    height: 35,
  },
  backButtonWrapper: {
    position: 'absolute',
    top: '6%',
    left: '1%',
    width: 35,
    height: 35,
  },
  backButton: {
    width: 35,
    height: 35,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'white'
  },
  datePicker: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold'
  },
  datePickerButton: {
    height: 35,
    width: 35,
    margin: 5
  },
  calendarRow: {
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
    height: '10%',
    borderBottomColor: 'white',
    borderBottomWidth: 0.2,
    paddingLeft: 20,
    paddingRight: 20,
  },
  calendarCell: {
    color: 'white',
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    width: '30%'
  },
  confirmedCalendarRow: {
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
    height: '10%',
    borderBottomColor: 'white',
    borderBottomWidth: 0.2,
    backgroundColor: '#ffd000',
    paddingLeft: 20,
    paddingRight: 20,
  },
  confirmedCalendarCell: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    width: '30%'
  },
  bold: {
    fontWeight: 'bold',
  },
  disabledButton: {
    opacity: 0.3,
  },
  subscriptionPlanGroup: {
    width: '100%',
    marginTop: '5%',
  },
  subscriptionPlanHeader: {
    flexDirection: 'row',
    textAlignVertical: 'bottom',
  },
  subscriptionPlanTitle: {
    color: '#ffd000',
    fontSize: 24,
    fontWeight: 'bold',
    textAlignVertical: 'bottom',
  },
  subscriptionPlanDuration: {
    color: '#ffd000',
    fontSize: 12,
    fontWeight: 'bold',
    textAlignVertical: 'bottom',
    paddingBottom: 4,
  },
  subscriptionPlanDetailCell: {
    color: 'white',
    fontSize: 16,
    width: '30%',
    flex: 1,
  },
  personalTrainingRowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: '5%',
    paddingLeft: '5%',
    paddingRight: '5%',
    borderTopColor: 'white',
    borderTopWidth: 0.2,
  },
  personalTrainingSessionCell: {
    color: 'white',
    fontSize: 16,
    flex: 3,
    fontWeight: 'bold',
  },
  personalTrainingDetailCell: {
    flexDirection: 'row',
    color: 'white',
    fontSize: 14,
    textAlignVertical: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    flex: 2,
  },
  currency: {
    color: 'white',
    fontSize: 9,
    width: '15%',
    textAlignVertical: 'bottom',
    marginLeft: 2,
    paddingBottom: 4,
  },
  rowGroup: {
    flexDirection: 'row',
    flex: 1,
  },
  rowGroup2: {
    flexDirection: 'row',
    backgroundColor: 'blue',

  },
  rightAligned: {
    textAlign: 'right',
  },
  silhouetteContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-start'
  },
  silhouette: {
    width: 15,
    height: 40,
  },
  inputButton: {
    height: '2%',
    width: '90%',
    minWidth: '30%',
    borderColor: 'gray',
    borderWidth: 3,
    borderRadius: 40,
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
    marginBottom: 10,
  },
  loginButtonWrapper: {
    width: '30%',
    height: '10%',
  },
  loginButton: {
    width: '100%',
    height: '100%',
  },
});
