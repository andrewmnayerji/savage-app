### Publishing a new version
This will cause OTA updates for the users. No need to update binaries on apple and google store.
```
expo publish
```

### Building an Android APK
This is needed for initial deployment or if something gets messed up
```
expo build:android -t apk
```

### Building an iOS binary
This is needed for initial deployment or if something gets messed up
```
expo build:ios -t archive
```

### Uploading to the App Store and Google Play Store
https://docs.expo.io/distribution/uploading-apps/
