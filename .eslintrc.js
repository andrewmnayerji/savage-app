module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    jest: true,
  },
  rules: {
    'comma-dangle': 'off',
    'func-names': 'off',
    'global-require': 'off',
    'guard-for-in': 'off',
    'import/named': 'off',
    'import/prefer-default-export': 'off',
    'no-param-reassign': 'off',
    'no-restricted-syntax': 'off',
    'no-unused-expressions': 'off',
    'react/jsx-filename-extension': 'off',
    'react/style-prop-object': 'off',
  },
  globals: {
    fetch: false
  }
};
